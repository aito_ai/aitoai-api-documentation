# API:Predict #

## Concept

The predict API is used to predict the likelihood of a feature given a hypothesis


## API end point
/api/\_predict

## Normal Predict Query

Given a hypothesis that the customer id is 5, we would like to predict the

![alt text](images/predict_simple.png)

    {
      "context": "impressions",
      "q" : [
        {"where":"customer", "is":5}
      ],
      "predict":"query"
    }

## Result

The above query would return an array list of results sorted in descending order of the probability:

    [
      {
        "probability" : 0.16895938956783463,
        "variable" : {
          "field" : "query",
          "feature" : "phone",
          "id" : "query:phone"
        },
        "why" : {
          "value" : 0.16097046328739992,
          "type" : "product",
          "factors" : [ {
            "value" : 0.3900037598620147,
            "type" : "norm",
            "why" : "exclusiveness"
          }, {
            "value" : 0.3946360153256705,
            "type" : "probability",
            "why" : "variable base probability"
          }, {
            "value" : 1.3421881205611745,
            "type" : "relation_lift",
            "relatedVariable" : "customer.tags:40s",
            "why" : "1.342x likelihood, when customer.tags:40s"
          }, {
            "value" : 0.7792329205930437,
            "type" : "relation_lift",
            "relatedVariable" : "customer.tags:nyc",
            "why" : "0.779x likelihood, when customer.tags:nyc"
          } ]
        }
      },
      {
        "probability" : 0.1389572159372041,
        "variable" : {
          "field" : "query",
          "feature" : "best",
          "id" : "query:best"
        },
        "why" : {
          "value" : 0.1371546207032386,
          "type" : "product",
          "factors" : [ {
            "value" : 0.3900037598620147,
            "type" : "norm",
            "why" : "exclusiveness"
          }, {
            "value" : 0.2260536398467433,
            "type" : "probability",
            "why" : "variable base probability"
          }, {
            "value" : 1.7268432887362426,
            "type" : "relation_lift",
            "relatedVariable" : "customer.tags:40s",
            "why" : "1.727x likelihood, when customer.tags:40s"
          }, {
            "value" : 0.9009013094915124,
            "type" : "relation_lift",
            "relatedVariable" : "customer.tags:nyc",
            "why" : "0.901x likelihood, when customer.tags:nyc"
          } ]
        }
      },
      {
        "probability" : 0.13709826505695993,
        "variable" : {
          "field" : "query",
          "feature" : "laptop",
          "id" : "query:laptop"
        },
        "why" : {
          "value" : 0.13729551707338436,
          "type" : "product",
          "factors" : [ {
            "value" : 0.3900037598620147,
            "type" : "norm",
            "why" : "exclusiveness"
          }, {
            "value" : 0.3103448275862069,
            "type" : "probability",
            "why" : "variable base probability"
          }, {
            "value" : 1.1343394901676636,
            "type" : "relation_lift",
            "relatedVariable" : "customer.tags:nyc",
            "why" : "1.134x likelihood, when customer.tags:nyc"
          } ]
        }
      },
      ...
    ]

Each result tuple contains:
  * _probability_: tells the likelyhood of the predicted variable given a context. For example, from the first result tuple, we can get the conditional probability P(query:phone|customer:5) = 0.16895938956783463
  * _variable_: contains the predicted variable information:
    * _field_: column name
    * _feature_:
    * _id_:
  * _why_: reveals aito's reasoning behind the predicition
    * _value_ : the base probability of the predicted variable. For example, from the first result tuple, we can get the base probability P(query:phone) = 0.16097046328739992
    * _type_ : ??
    * _factors_: contributing factors to the final probability
      * _value_: ??????
      * _type_
      * _why_

It is possible to combine multiple hypothesis properties using "_**and**_" and "_**or**_" keyword such as

    {
      "where": {
        "and": [
          { "field": "customer", "is": 4 },
          { "field": "prevProduct", "is": 0}
        ]
      }
    }

    {
      "where": {
        "or": [
          { "field": "customer", "is": 4 },
          { "field": "customer", "is": 5}
        ]
      }
    }

## Exclusiveness

Predictions are exclusiveness by default. This means that aito assumes the each field only contains one feature. For example, the category of a product is either laptop, phone, or PC and hence, we can use exclusiveness.
However, a product might have different tags. For instance, the tags can be "ios phone premium". In this case we should not use exclusiveness.
For example, if we want to predict tags of a product whose title is "samsung galaxy s9", we can use the following query:

    {
      "context": "products",
      "q" : [
        {"where":"title", "is":"samsung galaxy s9"}
       ],
       "exclusive":false,
       "predict":"tags"
    }