# API:Explain #

## Concept

The explain API is used to explain the causality of a hypothesis to a field. For instance, given a product title, we would like to see how it would affect the tags of the product

## API end point
/api/\_explain

## Normal Explain Query

Let's assume we have a product whose title is "leather protection", we would like to see how would this affect the product's tag. This can be done by using the **API:Explain**

    {
      "context": "impressions",
      "q": [
        { "where": "title", "is": "leather protection" }
      ],
      "explain": "tags"
    }

## Result

The above query would return an array list of results explaning how features in title is leather protection affecting the tags:

    [
      {
        "what" : "tags:cover",
        "lift" : 2.309631972937736,
        "when" : "title:leather"
      },
      {
        "what" : "tags:cover",
        "lift" : 2.309631972937736,
        "when" : "title:protect"
      }
    ]

Each result tuple contains:
  * when: tells the component of the hypothesis affecting the target field
  * what: the target field value
  * lift: the multiplier measurement of the conditional probability to the base probability. For example, in the first tuple, lift = 2.309631972937736 means that the conditional probability P(tags:cover|title:leather) = 2.309631972937736 * P(tags:cover). In other word, when title is leather, it is 2.3 times more likely that tags would be cover.

