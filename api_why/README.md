# API:Why #

## Concept

The Why API is used to investigate on a specific field or a feature. The Why API can return how different fields or some designated fields can affect the investigating field or feature.

## API end point
/api/\_why

## Investigating a field

Let's assume we want to see what affects the customer to click a product, we can use the **API:Why** to solve this problem by the following query:

    {
      "context": "impressions",
      "why": "click"
    }

The above query would return an array list of results explaning how different features can affect clicking:

    [
      {
        "what" : "click:true",
        "lift" : 1.6850431171980644,
        "when" : "product.description:appl"
      },
      {
        "what" : "click:false",
        "lift" : 0.9385217715335071,
        "when" : "product.description:appl"
      },
      {
        "what" : "click:true",
        "lift" : 1.6836750923856951,
        "when" : "product.title:appl"
      },
      {
        "what" : "click:false",
        "lift" : 0.9386445429910273,
        "when" : "product.title:appl"
      }
    ...

Each result tuple contains:
  * when: a feature that affect the investigating field
  * what: investigating field value
  * lift: the multiplier measurement of the conditional probability to the base probability. For example, in the first tuple, lift = 1.6850431171980644 means that the conditional probability P(click:true|product.description:appl) = 1.6850431171980644 * P(click:true). In other word, when there is a word "apple" in the product description, the customer would click it 68% more often than average.

## Investigating a feature

When investigating a field, aito would investigate all field's value. In the example above, since field "_click_" is of type boolean, aito would investigate both its value: true and false. We can choose to investigate on only one value, for example, "_click_" is true by using the following query:

    {
      "context": "impressions",
      "why": { "field": "click", "is": true }
    }

Similarly, we can investigate on fields of different types, for example, string:

    {
      "context": "impressions",
      "why": { "field": "tags", "is": "premium"}
    }

## Investigating based on some field

It is possible to investigate a field based on some designated fields. For example, we would want to investigate "_query_" based on "_customer_". The query would be:

    {
      "context": "impressions",
      "why":"query",
      "basedOn":["customer"]
    }

This query would yield a list of how different value of customer affecting the query:

    [
      {
        "what" : "query:cheap",
        "lift" : 4.081810286378045,
        "when" : "customer:0"
      },
      {
        "what" : "query:cheap",
        "lift" : 4.081810286378045,
        "when" : "customer.id:0"
      },
      {
        "what" : "query:cheap",
        "lift" : 4.081810286378045,
        "when" : "customer.name:anne"
      },
      {
        "what" : "query:iphon",
        "lift" : 4.081810286378045,
        "when" : "customer:2"
      },
      {
        "what" : "query:iphon",
        "lift" : 4.081810286378045,
        "when" : "customer.id:2"
      },
      ...

