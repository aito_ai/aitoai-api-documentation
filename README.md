# aito.ai - release api documentation  #

This repository contains a detailed documentation into aito.ai's APIs. You can find information of these following topics:

1. API's concept
2. Operations that can be done with each API
3. Formulating a query for an operation
4. Understanding the query's result

## Disclaimer:

This documentation for the released API is still work in progress. There are segments, which either reflect the earlier development API version, or which are untested and may not work.

The release API is not frozen yet.

## Structure

### 1. Example dataset

This repository contains a sample [ecommerce dataset](data) that has three tables and a schema for these three tables:

1. [Customers](data/customers.json)
2. [Products](data/products.json)
3. [Impressions](data/impressions.json)
4. [Schema](data/schema.json)

This dataset will be used for the example queries

### 2. APIs'documentation:

List of aito.ai's APIs:

1. [Schema](api_schema)
2. [Data](api_data)
3. [Find](api_find)
4. [Relate](api_relate)
5. [Predict](api_predict)

Each directory will contains instructions and example queries' content.

## Preparation

You can run the queries using aito.ai's website interface (TODO) or any REST Client such as [Postman](https://www.getpostman.com/) or [Insomnia](https://insomnia.rest/) on the aito training environment using:

1. Environment URL: https://aito-training.api.aito.ninja
2. Content-type: _application/json; charset=utf-8_
3. x-api-key: _cHOhgijfhT9ouDu7VssDj1CfvxyChBp49jMueZUc_ (read-only key)

or your own provided enviroment.

If you use Postman, it will look something like this:

![postman intro](images/postman_intro.png)

## aito.ai - the core concepts ##

aito is an synthesis of a database and AI technology, and as such, it introduces concepts and idea
from both of these realms. Some of database concepts and AI concepts have different names, yet are essentialy conceptually very similar or the same. In this chapter, we introduce the core concepts of the aito intelligence layer, and we describe how they relate to the terms existing elsewhere.

The basic concepts relating to the data are following:

* *Table*. Aito is a database like entity, that stores its data in relational tables consisting of rows and  columns, a bit like an SQL database.

* *Schema*. Each table has a strict schema, as in an SQL database. The schema contains the column names and value types, plus additional metainformation about linking, and the analysis of column values.

* *Values*. The table - of course - contains values at the intersection of each row and column. Strings, Integers, Long integers and double precision floating points are supported, as well as their optional versions.

* *Analyzers* and *features*. Aito does do the statistical reasoning at the level of values, but at reasons at the level of 'binary features'.

    * Specialized analyzers are used to decompose potentially large values (e.g. long text values) into individual binary features.
    * For example, if field 'age' has a value 54, this can be turned into feature age:54. If a field 'text' value 'horses are running' is analyzed with English language analyzer, it can be turned into features text:'hors' and text:'run'. The analyzers do not just split the text into words, but it can also drop overly common words, and reduce the words into their normal forms.
    * Analyzers may be familiar from search engines like Lucene or its derivatives like ElasticSearch and Solr.
    * While the main data is stored in a table, the features are stored in a separate feature dataframe

The concepts related to the use of aito are following:

+ Context. All aito's operations are done in a context. Context define a kind of a scenario, in which we there are knowns, and there unknowns.
    * Context has always some examined 'root table'. For example: this table may be the impression table.
    * Context has some knowns. For example, in the impression context, we may know the user,
      and we may know the page the user is int.
    * The context often has unknowns, which we haven't specified. For example, we may not know, whether
      the displayed content was clicked.

+ Relation. in aito, relation refers to the statistical examination of a set of features.
    * For example, the relation (name:'playstation', productCategory:electronics') is used to
      examine playstation's statistical relationship with the 'electronics' product category.
    * Also, the relation (user:'bob', product.category:'laptop', click:true) can be used to
      examine Bob's preference for laptops.
    * Aito often examines hundreds or thousands of relationships per query to find interesting
      statistical relationships that may help to provide results.

## Feedback, reporting bugs or inconsistencies

We take quality seriously and aim to perfect our software both for errors as well as usability issues. However, we're still in pilot phase, so we're making improvements and tweaks both to the core-functionality as well as the APIs. This means that sooner or later something
is bound to go out or order.

If you run into problems, please let us know about it, and we'll fix it ASAP. You can contact us on:

* Email: You can send bug reports to quality@aito.ai, please include version information and reproduction steps in the email. Please also leave your contact details, so we can be in contact with you for more information, or if you want to be notified on the status.

* Slack. We're opening a [public slack channel](https://aitoai.slack.com), where you can report bugs, ask questions or in general discuss anything product related. Please send and email with your contact details to slack@aito.ai to receive an invitation. Email domains aito.ai and futurice.com are accepted by default.