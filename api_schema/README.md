# API:Schema #

## Concept
An aito's schema is similar to a SQL's schema.
The schema contains the tables' name, columns' name and values' type.
It also contains additional meta-information about linking, and the analysis of column values. This is important in order for aito to perform well. These meta-information will be further explained in below sections.
API: Schema is used to manage the schema.


## API end point
/api/\_schema


## Structure

Available operations:

1. Populate a new schema 
2. Delete an existing schema 
3. Delete an existing table (delete both information from the schema and data content from the database) 

### aito's schema structure:

1. Example:

    
        {
          "schema": {
    
            "customers": {
              "type":"table",
              "columns": {
                "id"   : {"type": "Int" },
                "name" : {"type": "String" },
                "tags" : {"type": "Text", "analyzer": "Whitespace" }
              }
            },
        
            "products": {
              "type":"table",
              "columns": {
                "id"          : { "type": "Int" },
                "title"       : { "type": "Text",  "analyzer": "English" },
                "tags"        : { "type": "Text",  "analyzer": "Whitespace" },
                "price"       : { "type": "Decimal"},
                "description" : { "type": "Text",  "analyzer": "English" }
              }
            },
    
            "impressions": {
              "type":"table",
              "columns": {
                "prevProduct" : { "type": "Int", "nullable":true, "link": "products.id" },
                "customer" :    { "type": "Int", "link": "customers.id" },
                "product" :     { "type": "Int", "link": "products.id" },
                "query" :       { "type": "Text", "analyzer": "English" },
                "click" :       { "type": "Boolean" }
              }
            }
    
          }
        }
    
2. Explanation:
  
    * The above schema defines 3 tables: customers, products, impressions and its contents
    * "schema" : a map from table names to table definitions.
        * "type": the type of the schema object. Must be 'table', as only 'table' is currently supported.
        * "columns": a map from column names to colum defitions
            * "type": type of variable in this column ("Int", "String", "Decimal", "Boolean" or "Text")
            * "nullable": true, if the column value can be 'null'
Optional(Int) type
           * "analyzer": type of support analyzer to split values into features ("Default", "No", "Whitespace", "Standard", "Finnish", "English", "Swedish", "German")
              * Only supported for columns of type "Text". 
              * E.g: the product description is in English and can use the English analyzer
        * "link": link this column to another column (can be from this table or another table). This helps aito to take linked column into consideration when performing smart operations
            * E.g: the customer in the impressions table can be link to id in customers table so when you perform smart operations in the impressions table, aito takes into account customers' information


### Populate a new schema
To populate a new schema into aito, send a PUT request to 

    <_your_env_url_>/api/schema/\_mapping

with the provided read-write api_key and the schema in json format as content of the request

The training environment has already been populate with the [schema](../data/schema.json)

### Delete an existing schema
**Note: This will delete the entire database as well**
To delete an existing schema in aito, send a DELETE request to 

    <_your_env_url_>/api/schema/\_mapping

with the provided read-write api_key and empty content


### Delete an existing table
To delete an existing table from aito's schema and from aito's database, send a DELETE request to 

    <_your_env_url_>/api/schema/<_table_name_>

with the provided read-write api_key and empty content


### Repopulate a deleted table

After deleting the table, you can repopulate a new schema with the deleted table and repopulate the deleted table to recover it


**NOTE**: Current version of aito does not support editing schema (E.g: update new link, update column type). Also, populating a new schema without table(s) does not delete the table(s) from the database
The best practice is to delete the whole database (by delete schema), repopulate the updated schema, and repopulate the data content
This will be improved soon in the future