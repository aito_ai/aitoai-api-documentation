# API:Relate #

*TODO*

## Concept

The relate API provides statistical information regarding the relationship between a pair of features (columns in tables). 
It can be used as an analyzation tool to for example find causation and correlation. 
In the following section, we will go through the relate API query and interpreting the result

**NOTE**: Currently does not support relate 2 features in the same table


## API end point
/api/\_relate

## Query

[Example Query](queries/relate_simple.json)
[Example Result](results/relate_simple.json)

![alt text](images/relate_simple.png)

The above query would relate the click is true event with the product's title-field features.

## Result

The above query would return result as a list of statistical information in the following format:

	[
    {
        "mutualInformation": 0.00649973689378424,
        "samples": 253,
        "variables": [
            {
                "feature": true,
                "field": "click",
                "conditional": [
                    {
                        "negationLift": 1.016326918876258,
                        "samples": 207,
                        "whenVariableStates": [
                            {
                                "index": 1,
                                "state": false,
                                "field": "product.title",
                                "feature": "appl"
                            }
                        ],
                        "frequency": 12,
                        "lift": 0.8180714753788391,
                        "probability": 0.06737059209002204
                    },
                    {
                        "negationLift": 0.9386445429910274,
                        "samples": 46,
                        "whenVariableStates": [
                            {
                                "index": 1,
                                "state": true,
                                "field": "product.title",
                                "feature": "appl"
                            }
                        ],
                        "frequency": 8,
                        "lift": 1.6836750923856951,
                        "probability": 0.13865559584352782
                    }
                ],
                "frequency": 20,
                "index": 0,
                "probability": 0.08235294117647059,
                "variableId": "click:true"
            },
            {
                "feature": "appl",
                "field": "product.title",
                "conditional": [
                    {
                        "negationLift": 1.0138744473678438,
                        "samples": 233,
                        "whenVariableStates": [
                            {
                                "index": 0,
                                "state": false,
                                "field": "click",
                                "feature": true
                            }
                        ],
                        "frequency": 38,
                        "lift": 0.9374489454380646,
                        "probability": 0.17018646557888162
                    },
                    {
                        "negationLift": 0.838562527314939,
                        "samples": 20,
                        "whenVariableStates": [
                            {
                                "index": 0,
                                "state": true,
                                "field": "click",
                                "feature": true
                            }
                        ],
                        "frequency": 8,
                        "lift": 1.727818838079856,
                        "probability": 0.3136718886339054
                    }
                ],
                "frequency": 46,
                "index": 1,
                "probability": 0.18154211640758147,
                "variableId": "product.title:appl"
            }
        ],
        "naiveEntropy": 1.0938551664181706,
        "entropy": 1.0806669914428908
    },
    ...


It provides a lot of useful statistical information. Let's go through each keyword

### 1. Overview Information

Aito provides some overview information regarding the relationship betweens the pair of features to be related 

#### 1.1 Mutual Information

		"mutualInformation": 0.00649973689378424

_Mutual Information_ measures the mutual dependence between the two features. In simple words, this statistical feature tells how much information these two features share. 
If two features are independent and knowing one feature would not reveal any information about the other, the mutual information would be 0.

#### 1.2. Samples:
	
Number of samples were considered in this context. 

#### 1.3. Naive Entropy and Entropy
__*TODO*__: Make this understandable

* The relation's naiveEntropy is simply the sum of the variable entropies. If the entropies of click:true and product.discount:true are 0.1, the relation's 'naive entropy' is 0.2.

* Relation's entropy describes the estimated 'real entropy' of the relationship H(R) = H((click:true, product.discount:true)). This entropy describes the minimum amount bits, that the variables A, B, ... N can be compressed on a row level, when examined together. This 'real entropy' is always either equal or lower than the 'naive entropy'. If the variables are independent, the relation's entropy is equal to its naive entropy. If the real entropy is lower, it means that variables are dependent.

* The mutual information is the difference between the naive entropy and real entropy. E.g. let's say, that click:true and product:discount:true share 0.01 bit of information. This means, that the relation's entropy is 0.19, while the naive entropy is 0.2 and the mutual information is 0.01. This also implies, that if the dicount variable is known, the click variable has only H(click:true|product.discount:true)=0.09 bits of entropy. Having 10 perfectly independent 'teller' variables/relationships, that share 0.01 bits of information with the click variable would drop the click variable condititional entropy to 0, which would mean that the click variable could always be perfectly predicted, when the teller variables are known. Mutual information is useful for measuring the strenght of discrete variables' statistical relationship, because...
	* with discrete variables traditional metrics like correlations don't work very well
	* mutual information is extremely fast to calculate for binary features
	* mutual information is pretty much the mathematically correct metric to use and it has a clean theorethical interpretation as the geometric mean lift (gml(A;B)=2^I(A:B)) in the total likelihood of events
	* and it behaves linearly so that 0.01 bits of mutual information from one source and 0.01 bits of mutual information of another brings (at most) 0.02 bits of mutual information together. For example, the mutual informations probabilistic version 'geometric mean lift' doesn't have this property.

### 2. Relation Information:

The list following ```variables``` contains statistical information about a pairs of feature that was related by aito. The above example show the pair of _click:true_ (feature _click_ has value true) and _product.title:appl_ (feature title in _product_ which was linked to _products_ table is _appl_)

#### 2.1 Feature of interest information

	{
        "feature": true,
        "field": "click",
        "conditional": [
            ...
        ]
        "frequency": 20,
        "index": 0,
        "probability": 0.08235294117647059,
        "variableId": "click:true"
    },

* _feature_, _field_, _variableId_: In this case, the feature of interest in "click:true"
* _frequency_: Number of samples satisfying the feature of interest. In this case, _click:true_ occured 20 times among 253 considered samples
* _probability_: Aito estimation of base probability of _click_ is true P(_click_ is true) happening 



#### 2.2 Conditional feature information

	conditional": [
	    {
	        "negationLift": 1.016326918876258,
	        "samples": 207,
	        "whenVariableStates": [
	            {
	                "index": 1,
	                "state": false,
	                "field": "product.title",
	                "feature": "appl"
	            }
	        ],
	        "frequency": 12,
	        "lift": 0.8180714753788391,
	        "probability": 0.06737059209002204
	    },
	...
	]

The list following ```conditional``` contains statistical information describes the likelyhood of the feature of interest given a conditional feature. 

__*Conditional Probability*__: The conditional probability of the feature of interest given a conditional feature P(_feature_of_interest_ | _conditional_feature_) can be interprete as the likelihood that _feature_of_interest_ would happen given that a _conditional_feature_ happens.

More information about conditional probability can be found in [Wiki](https://en.wikipedia.org/wiki/Conditional_probability)

__*Keywords*__:

* _whenVariableStates_: Describe information of the conditional feature
	* Interpretation: _field_ = _feature_ is _state_
	* In this case, the conditional feature is: product title is not appl (_product.title:appl = false_)
* _samples_: 
	* Number of samples satisfying the conditional feature
	* In this case, 207 samples satisfying _product.title_ is _appl_
* _frequency_:
	* Number of samples satisfying the feature of interest and the conditonal feature
	* In this case, 12 samples satisfying _click_ is _true_ and _product.title_ is _appl_
* _lift_ and _negationLift_: 
	* _lift_ is the multiplier measurement of the conditional probability to the base probability. In this case, ```lift = 0.81``` means that the conditional probabiliy  P(_click_ is true | _product.title_ = apple is false)
	* _negationLift_ is the reverse of lift that is measurement of the conditional probability of feature of interest not happening given conditonal feature. In this case, it is P(_click_ = false | _product.apple_ = false)
	* Interpretation: In this case, ```lift = 0.81``` means that when the product title is not apple (_product.title_ = apple is false), its is 20% less likely that the customer would click it (_click_ is true). On the other hand, ```negationLift": 1.01``` means that when the product title is not apple, it is 1% more likely that the customer would not click it. 
* _probability_:
	* The estimated conditional probability P(_feature_of_interest_ | _conditional_feature_)
	* In this case, the estimated probability ```P(_click_ is true | _product.title_ = apple is false) = 0.06``` which is lower than the base probabily ```P(_click_ is true) = 0.08```

### 2. Further usecase of relates:

#### 2.1 Exploring relations between multiple features

It is possible to use relate API to relate between multiple features, without knowing the exact feature value to relate to. 

[Example Query](queries/relate_multiple_no_value.json)
[Example Result](results/relate_multiple_no_value.json)

In this case, we want to examine the result between 'product', 'prevProduct', and 'click'. Aito.ai would return the statistical results of pairs of value, sorted by mutual information. 
Based on the result:

    {
        "mutualInformation": 0.00649973689378424,
        "samples": 253,
        "variables": [
            {
                "feature": "appl",
                "field": "product.title",
                "conditional": [
                    ...,
                    {
                        "negationLift": 0.838562527314939,
                        "samples": 20,
                        "whenVariableStates": [
                            {
                                "index": 1,
                                "state": true,
                                "field": "click",
                                "feature": true
                            }
                        ],
                        "frequency": 8,
                        "lift": 1.727818838079856,
                        "probability": 0.3136718886339054
                    }
                ],
                "frequency": 46,
                "index": 0,
                "probability": 0.18154211640758147,
                "variableId": "product.title:appl"
            }...
    }

We can see that there is also high mutual information between a product's title containing 'appl' and 'click'. More specifically, when a product's title has 'appl', it is 1.7 times more likely that it will be clicked

We can also relate a feature to a pair of features. 

[Example Query](queries/relate_feature_to_pair.json)
[Example Result](results/relate_feature_to_pair.json)

In this case, aito would examine the relationship between a feature values and a pair of feature values among 'prevProduct', 'query', and 'click'


#### 2.2 More complex relate query

Using the relation tuple 'and' similar to the find API, it is possible to examine more sophisticated relationship between features

[Example Query](queries/relate_complex.json)
[Example Result](results/relate_complex.json)

In this case, aito.ai will examine the statistical relationship between between 3 variables, of which 2 are composite variables formed with 'and'-operation.