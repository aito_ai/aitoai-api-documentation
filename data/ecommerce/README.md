# aitoai-ecommerce-dataset

This repository contains the ecommerce dataset described in the documentation project (https://bitbucket.org/aito_ai/aitoai-documentation).

The data in the example is purely fictional, and has been created for demonstration purposes only.

Please feel free to comment the code and send us pull requests or issues, if you find problems with the code.

This code is licensed under the MIT license, and you are free to copy it, modify it and distribute it as you see fit.
