# API:Find #

## Concept

The find API is a powerful tool that can be used to perform smart search operations in the aito database

It allows you to:

* Filter the rows by some properties
	E.g. find all products in the products, where _category_ is _phone_
* Refining the results:
	E.g.

	* sort the products by price in descending order
	* only return the first result in all results

* Return results by the probabilty of a desired property
	E.g. Return the products by the probability that it will be clicked, given that the user searched for laptop

* Similarity search based on IDF similarity metrics
	E.g. find similar products given some criteria

In the following section, we will go through each usage by definition and by example. The example are based on our sample ecommerce dataset. More information of the ecommerce dataset can be found [here](../data/ecommerce/README.md)


## API end point
/api/\_find


## 1. Filter the rows by some properties

To formulate a query that filters the rows by some properties, follows these steps:

1. Define the table to search and set it as the context:

	E.g: choosing table products

		"context": "products"

2. Choose some properties:

	The property can be defined by using operation word. More information on property can be found [here](../misc/property/README.md)

	E.g:
		* products whose tags matches 'phone'
		* products whose price is 800

The [query formulated] should be:

    {
      "context": "products",
      "q" : [
         { "where" : { "field": "tags" , "matches": "phone" } }
         { "where" : { "field": "price", "is"     :  800 } }
      ]
    }

This query will returns products whose tags containing the word 'phone' or words that are similar to 'phone' and the product price is 800 in the follwing format:

	{
            "count" : 1,
            "last" : 0,
            "totalCount" : 1,
            "first" : 0,
            "rows" : [
            	{
					"rowIndex" : 0,
					"hitIndex" : 0,
					"row" : {
						"description" : "apple iphone is a premium phone",
						"tags" : "ios phone premium",
						"price" : 800.0,
						"id" : 0,
						"title" : "apple iphone"
              		}
            	}
            ]
    }

where as:

* *count*: Number of results returned (can be smaller than the total number of results)
* *first*: hitIndex of first result
* *last*: hitIndex of last result
* *totalCount*: Total number of results
* *rows*: satisfied results' content:

	* *rowIndex*: The order of this row in the actual table
	* *hitIndex*: The order of this row in returned results
	* *row*: Actual row content

If there is no product that can satisfy these conditions, aito returns 0 count and empty 'rows':

	{
		"count": 0,
		"totalCount": 0,
		"rows": []
	}


The query above can be written as combined property:

	{
		"context": "products",
		"q" : [
			{
				"where": {
					"and": [
			            {"field": "tags",  "matches": "phone" } ,
			            {"field": "price", "is"     :  800 }
	            	]
		        }
			}
		]
    }

You can read more about different types of properties and how to combine them to get the expected result [here](../misc/property/README.md)

## 2. Refining the results

### 2.1 Order the results by a field (a column in a table)

To order the rows by a field, add the '_orderBy_' refining keyword and the wanted field to the query and define the order rules (ascending/descending)

    {
      "context": "products",
      "q" : [
         { "where": { "field": "tags", "matches": "phone" } }
         { "orderBy": "price", "asc": false }
      ]
    }


The results would be:

	{
        "count" : 3,
        "last" : 2,
        "totalCount" : 3,
        "first" : 0,
        "rows" : [ {
          "rowIndex" : 0,
          "hitIndex" : 0,
          "row" : {
            "description" : "apple iphone is a premium phone",
            "tags" : "ios phone premium",
            "price" : 800.0,
            "id" : 0,
            "title" : "apple iphone"
          },
          "sort" : "800.0"
        }, {
          "rowIndex" : 1,
          "hitIndex" : 1,
          "row" : {
            "description" : "samsung s8 is a premium phone with all sorts of features ",
            "tags" : "android phone premium",
            "price" : 600.0,
            "id" : 1,
            "title" : "samsung s8"
          },
          "sort" : "600.0"
        }, {
          "rowIndex" : 2,
          "hitIndex" : 2,
          "row" : {
            "description" : "huawei is an affordable android phone",
            "tags" : "android phone affordable",
            "price" : 150.0,
            "id" : 2,
            "title" : "huawei honor"
          },
          "sort" : "150.0"
        } ]
    }

The format of the result is similar to the normal result with the extra field '_sort_' containing the value of the colum which was used for sorting.

### 2.2 Return only a subset of the results

In situation where there are too much results, it is helpful to return only a certain amount of results. In order to do so, add the '_from_' and '_take_' refining keyword.

	{
      "context": "products",
      "q" : [
        { "where": { "field": "tags", "matches": "phone" } }
        { "orderBy": "price", "asc": false },
        { "from": 1, "take": 1}
      ]
    }

The clause ```{ "from": 1, "take": 1}``` signals aito to returns only 1 result, starting from hitIndex 1. The result would be:

	{
        "count" : 1,
        "last" : 1,
        "totalCount" : 3,
        "first" : 1,
        "rows" : [ {
          "rowIndex" : 1,
          "hitIndex" : 1,
          "row" : {
            "description" : "samsung s8 is a premium phone with all sorts of features ",
            "tags" : "android phone premium",
            "price" : 600.0,
            "id" : 1,
            "title" : "samsung s8"
          },
          "sort" : "600.0"
        } ]
    }

## 3. Return results by the probabilty of a desired property

Given some hypothesis, aito can return the results by descending order of the probabilty of a desired property. This is done by using the keyword "_**orderByContextProbability**_"

For example, we want to find some products and our desired property is that the product will be clicked. From the context of '_impressions_' table, the desired property would be:

	{ "field": "click", "is": true }

We need some hypothesis in order for aito to return the probabilty.
For example, the hypothesis can be that the customer searched for a laptop:

	{ "where": "query", "is": "laptop" }

This hypothesis must be put before what we want to find in the query:

	{ "where": "query", "is": "laptop" }
	{ "find": "product" }

We now have enough information to form the query:

	{
    	"context": "impressions",
     	"q" : [
        	{ "where": "query", "is": "laptop" },
        	{ "find": "product" },
        	{ "orderByContextProbability": { "field": "click", "is": true } }
      	]
    }

The result would be all products in descending order of the aito estimated probability that the product would be clicked:

	{
		"count" : 11,
		"last" : 10,
		"totalCount" : 11,
		"first" : 0,
		"rows" : [
			{
				"rowIndex" : 3,
				"hitIndex" : 0,
				"row" : {
					"description" : "apple macbook is the top laptop in the market",
					"tags" : "macosx laptop premium",
					"price" : 1500.0,
					"id" : 3,
					"title" : "apple macbook"
				},
				"probability" : 0.9653078661364864
			},
			{
				"rowIndex" : 4,
				"hitIndex" : 1,
				"row" : {
					"description" : "hp is spectre is a premium laptop, that is compatible with phones",
					"tags" : "windows laptop premium",
					"price" : 1500.0,
					"id" : 4,
					"title" : "hp spectre"
				},
				"probability" : 0.02737653282771936
			},
			{
				"rowIndex" : 5,
				"hitIndex" : 2,
				"row" : {
					"description" : "lenovo idea pad is an affordable white laptop",
					"tags" : "windows laptop affordable",
					"price" : 400.0,
					"id" : 5,
					"title" : "lenovo ideapad white"
				},
				"probability" : 0.0052123372763036415
			},
		...
	}

The format of the result is similar to the normal result with the extra field '_probability_' containing the estimated probabilty of desired context.

_**NOTE**_: In the example above, aito will return all products in the products table. Hence, the returned result can be enormous in many scenarios. To fix this problem, you can use the "_from take_" clause. For example, request aito to return only 3 products with the highest probability by using this query:

	{
		"context": "impressions",
		"q" : [
			{ "where": "query", "is": "laptop" },
			{ "find": "product" },
			{ "orderByContextProbability": { "field": "click", "is": true } },
			{ "from": 0, "take": 3}
		]
    }

You can futher refine the result by giving aito some condition property. For example, the products must be expansive, the price must be greater than 1000. The query would be:

	{
		"context": "impressions",
		"q" : [
			{ "where": "query", "is": "laptop" },
			{ "find": "product" },
			{ "where": "price", "gt": 1000 },
			{ "orderByContextProbability": { "field": "click", "is": true } },
			{ "from": 0, "take": 3}
		]
    }

_**NOTE**_: The condition property must be defined after "_find_" in the query.

Similar to normal find query, it is possible to combine different properties to get the desired result. For example:

	{
      "context": "impressions",
      "q" : [
        { "where":
        	{
				"or": [
		            {"field": "query", "is": "laptop" },
		            {"field": "query", "is": "tablet" }
            	]
	        }
	    },
        { "find": "product" },
        { "where":
        	{
				"and": [
		            {"field": "price", "gt": 300 } ,
		            {"field": "description", "matches": "apple" }
            	]
	        }
	    },
        { "orderByContextProbability": { "field": "click", "is": true } }
      ]
    }


## 4. Return results by the probability of being similar to a given property

Using "_**orderByHitScore**_" combined with "_**similarity**_", aito can find results that are similar to given properties.
For example, we can find products whose title is similar to 'apple smartwatch' by using:

	{ "orderByScore":
		{ "similarity": { "field": "title", "is": "apple smartwatch"} }
	}

We can send this query to find API:

	{
		"context": "products",
		"q": [
			{ "orderByHitScore":
				{ "similarity": { "field": "title", "is": "apple smartwatch"} }
			}
		]
	}

The results would be all the products ranked in descending order of similarty score:


	{
        "count" : 10,
        "last" : 9,
        "totalCount" : 11,
        "first" : 0,
        "rows" : [ {
          "rowIndex" : 0,
          "hitIndex" : 0,
          "row" : {
            "description" : "apple iphone is a premium phone",
            "tags" : "ios phone premium",
            "price" : 800.0,
            "id" : 0,
            "title" : "apple iphone"
          },
          "score" : 0.10263094867381196
        }, {
          "rowIndex" : 3,
          "hitIndex" : 1,
          "row" : {
            "description" : "apple macbook is the top laptop in the market",
            "tags" : "macosx laptop premium",
            "price" : 1500.0,
            "id" : 3,
            "title" : "apple macbook"
          },
          "score" : 0.10168067530487676
        }, {
          "rowIndex" : 1,
          "hitIndex" : 2,
          "row" : {
            "description" : "samsung s8 is a premium phone with all sorts of features ",
            "tags" : "android phone premium",
            "price" : 600.0,
            "id" : 1,
            "title" : "samsung s8"
          },
          "score" : 0.08840981955792347
        }, {
          "rowIndex" : 2,
          "hitIndex" : 3,
          "row" : {
            "description" : "huawei is an affordable android phone",
            "tags" : "android phone affordable",
            "price" : 150.0,
            "id" : 2,
            "title" : "huawei honor"
          },
          "score" : 0.08840981955792347
        },
        ...
    	]


It is possible to combine the properties. For example, title is smartwatch and description is apple

	{
		"context": "products",
		"q": [
			{ "orderByHitScore":
				{ "similarity": {
					"and": [
						{ "field": "title", "is": "smartwatch" },
						{ "field": "description", "is": "apple" }
					]}
				}
			}
		]
	}


