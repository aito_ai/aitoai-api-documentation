# Operation #

## Concept

Property is a pivotal concept in aito.
Properties are used mostly to define the known conditions, in order to better predict the unknown, or used as conditions when perfoming smart search in the data base.
A property contains:

* column name (???)
* operation keyword (???)
* condition (???)

![alt text](images/property_example1.png)


## Combined property

Multiple properties can be combined together by relation keyword '_and_' and '_or_'.
For example:

	{ "and":
		[
        	{"field": "tags",  "matches": "phone" } ,
        	{"field": "price", "is"     :  800 }
    	]
	}

would combined 2 properties: tags matches phone and price is 800. When apply to a **API:FIND** query, for example:

	{
		"context": "products",
		"q" : [
			{
				"where": {
					"and": [
			            {"field": "tags",  "matches": "phone" } ,
			            {"field": "price", "is"     :  800 }
	            	]
		        }
			}
		]
    }

would return products that satisfy both property.

Similarly, using the '_or_' relation keyword:


	{
		"context": "products",
		"q" : [
			{
				"where": {
					"or": [
			            {"field": "tags",  "matches": "phone" } ,
			            {"field": "price", "is"     :  800 }
	            	]
		        }
			}
		]
    }

The above query when sent to **API:FIND** would return products that satisfy one of the two properties.

It is also possible to combine more then two properties, for example:

	{ "or":
		[
        	{"field": "tags",  "matches": "phone" } ,
        	{"field": "price", "is"     :  800 },
        	{"field": "title", "is"     :  "apple" }
    	]
	}

It is also possible to combine different relationships, for example:

	{ "or":
		[
			{ "field": "title", "matches": "apple iphone" },
			{ "and":
				[
					{"field": "price", "is": 800 },
        			{"field": "tags", "has": "phone" }
				]
			}
    	]
	}